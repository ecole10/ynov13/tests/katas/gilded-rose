# Gilded Rose

## Contexte

[Spécification](https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/main/GildedRoseRequirements_fr.md#sp%C3%A9cification-de-la-rose-dor%C3%A9e-gilded-rose)

## Choix

Pour ce TP, j'ai choisi le kata Gilded Rose, car il avait été abordé lors du cours d'architecture, mais avec une vision code first.
Grâce aux différentes méthodologies et pratiques apprises dans ce cours, j'aimerai l'aborder avec une approche orientée tests.

## Stratégie

Pour réaliser le kata, je vais d'abord sécuriser le comportement du code existant.
Pour cela, je vais me servir d'un golden master.
Puis, je vais écrire des tests unitaires pour mettre en évidence les règles métiers et pouvoir m'affranchir du golden master.
Enfin, je vais pouvoir refacto le code grâce à mon filet de sécurité. Pour finir, je vais pouvoir implémenter la nouvelle fonctionalité à l'aide de la méthode TDD. 

## Amélioration

[Liste des tickets](https://gitlab.com/ynov13/tests/katas/gilded-rose/-/issues?sort=created_date&state=all)

## Pré-requis

- [java](https://www.oracle.com/java/technologies/downloads/)
- [gradle](https://gradle.org/install/)

## Installation

```bash
gradle build
```

## Tests

```bash
gradle test
```

Mutation testing

```bash
gradle pitest
```

## Diagramme

![](./doc/diagramme.png)