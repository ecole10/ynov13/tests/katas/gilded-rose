package com.gildedrose.item

class ItemFactory {
    companion object {
        fun create(item: Item) : Item {
            return when {
                item.name.toUpperCase().contains(Sulfuras.name) -> Sulfuras()
                item.name.toUpperCase().contains(AgedBrie.name) -> AgedBrie(item.sellIn, item.quality)
                item.name.toUpperCase().contains(Backstage.name) -> Backstage(item.sellIn, item.quality)
                item.name.toUpperCase().contains(Conjured.name) -> Conjured(item.sellIn, item.quality)
                else -> Regular(item.name, item.sellIn, item.quality)
            }
        }
    }
}
