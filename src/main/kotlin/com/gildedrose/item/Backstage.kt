package com.gildedrose.item

class Backstage(sellIn: Int, quality: Int) : Irregular(name, sellIn, quality) {
    companion object {
        val name: String
            get() {
                return "BACKSTAGE PASSES"
            }
        const val DAYS_LEFT_TO_INCREASE_QUALITY_BY_TWO = 10
        const val DAYS_LEFT_TO_INCREASE_QUALITY_BY_THREE = 5
        const val QUALITY_AFTER_EXPIRATION = 0
    }

    override fun update() {
        decreaseSellIn()
        increaseQuality()

        if (isSellInLessThanOrEqualsDaysLeft(DAYS_LEFT_TO_INCREASE_QUALITY_BY_TWO)) {
            increaseQuality()
        }

        if (isSellInLessThanOrEqualsDaysLeft(DAYS_LEFT_TO_INCREASE_QUALITY_BY_THREE)) {
            increaseQuality()
        }

        decreaseQuality()
    }

    private fun decreaseQuality() {
        if (this.sellIn < SELL_EXPIRATION) {
            this.quality = QUALITY_AFTER_EXPIRATION
        }
    }

    private fun isSellInLessThanOrEqualsDaysLeft(daysLeft: Int) = this.sellIn <= daysLeft
}
