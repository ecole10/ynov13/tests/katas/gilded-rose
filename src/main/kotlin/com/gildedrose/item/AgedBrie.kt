package com.gildedrose.item

class AgedBrie(sellIn: Int, quality: Int) : Irregular(name, sellIn, quality) {
    companion object {
        val name: String
            get() {
                return "AGED BRIE"
            }
        const val DAY_FROM_INCREASE_QUALITY_BY_TWO = 0
    }

    override fun update() {
        decreaseSellIn()
        increaseQuality()

        if (isSellInExpired()) {
            increaseQuality()
        }
    }

    private fun isSellInExpired() = this.sellIn < DAY_FROM_INCREASE_QUALITY_BY_TWO
}
