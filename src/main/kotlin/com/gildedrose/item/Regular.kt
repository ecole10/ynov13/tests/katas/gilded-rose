package com.gildedrose.item

open class Regular(name: String, sellIn: Int, quality: Int) : Item(name, sellIn, quality), Updatable {
    companion object {
        const val QUALITY_MIN = 0
        const val SELL_EXPIRATION = 0
        const val QUANTITY_OF_DECREASE_SELL_IN = 1
        const val QUANTITY_OF_DECREASE_QUALITY = 1
    }

    override fun update() {
        decreaseSellIn()
        decreaseQuality()

        if (isSellOut()) {
            decreaseQuality()
        }
    }

    protected fun decreaseSellIn() {
        this.sellIn = this.sellIn - QUANTITY_OF_DECREASE_SELL_IN
    }

    protected open fun decreaseQuality() {
        if (this.quality > QUALITY_MIN) {
            this.quality = this.quality - QUANTITY_OF_DECREASE_QUALITY
        }
    }

    protected fun isSellOut() = this.sellIn < SELL_EXPIRATION
}
