package com.gildedrose.item

class Conjured(sellIn: Int, quality: Int) : Regular(name, sellIn, quality) {
    companion object {
        val name: String
            get() {
                return "CONJURED"
            }
        const val NUMBER_OF_DECREASE_QUALITY_THAN_REGULAR = 2
    }

    override fun update() {
        decreaseSellIn()
        decreaseQuality()

        if (isSellOut()) {
            decreaseQuality()
        }
    }

    override fun decreaseQuality() {
        if (this.quality > QUALITY_MIN) {
            this.quality = this.quality - NUMBER_OF_DECREASE_QUALITY_THAN_REGULAR * QUANTITY_OF_DECREASE_QUALITY
        }
    }
}
