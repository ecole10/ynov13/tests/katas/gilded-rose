package com.gildedrose.item

interface Updatable {
    fun update()
}
