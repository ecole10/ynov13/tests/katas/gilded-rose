package com.gildedrose.item

class Sulfuras : Item(name, sellIn, quality) {
    companion object {
        val name: String
            get() {
                return "SULFURAS"
            }
        val sellIn: Int
            get() {
                return 0
            }
        val quality: Int
            get() {
                return 80
            }
    }
}
