package com.gildedrose.item

abstract class Irregular(name: String, sellIn: Int, quality: Int) : Item(name, sellIn, quality), Updatable {
    companion object {
        const val QUALITY_MAX = 50
        const val SELL_EXPIRATION = 0
        const val QUANTITY_OF_DECREASE_SELL_IN = 1
    }

    protected fun decreaseSellIn() {
        this.sellIn = this.sellIn - QUANTITY_OF_DECREASE_SELL_IN
    }

    protected fun increaseQuality() {
        if (this.quality < QUALITY_MAX) {
            this.quality++
        }
    }
}
