package com.gildedrose

import com.gildedrose.item.Item
import com.gildedrose.item.ItemFactory
import com.gildedrose.item.Updatable

class GildedRose(var items: Array<Item>) {
    fun updateQuality() {
        for (item in items) {
            val specificItem: Item = ItemFactory.create(item)

            if (specificItem is Updatable) {
                specificItem.update()
            }

            item.quality = specificItem.quality
            item.sellIn = specificItem.sellIn
        }
    }
}
