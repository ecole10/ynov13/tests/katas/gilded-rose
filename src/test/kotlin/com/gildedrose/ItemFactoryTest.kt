package com.gildedrose

import com.gildedrose.item.*
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class ItemFactoryTest {

    @Test
    fun `Item name contains 'Sulfuras' should return Sulfuras object`() {
        // Arrange
        val item = Item("Sulfuras, Hand of Ragnaros", 0, Sulfuras.quality)

        // Act
        val result = ItemFactory.create(item)

        // Assert
        assertTrue(result is Sulfuras)
    }

    @Test
    fun `Item name contains 'Aged Brie' should return AgedBrie object`() {
        // Arrange
        val item = Item("Aged Brie", 0, 0)

        // Act
        val result = ItemFactory.create(item)

        // Assert
        assertTrue(result is AgedBrie)
    }

    @Test
    fun `Item name contains 'Backstage passes' should return Backstage object`() {
        // Arrange
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 0, 0)

        // Act
        val result = ItemFactory.create(item)

        // Assert
        assertTrue(result is Backstage)
    }

    @Test
    fun `Item name contains 'Conjured' should return Conjured object`() {
        // Arrange
        val item = Item("Conjured Mana Cake", 0, 0)

        // Act
        val result = ItemFactory.create(item)

        // Assert
        assertTrue(result is Conjured)
    }

    @Test
    fun `Item name not contains 'Sulfuras' or 'Aged Brie' or 'Backstage passes' should return Regular object`() {
        // Arrange
        val item = Item("Elixir of the Mongoose", 0, 0)

        // Act
        val result = ItemFactory.create(item)

        // Assert
        assertTrue(result is Regular)
    }
}
