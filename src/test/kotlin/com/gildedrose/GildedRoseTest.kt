package com.gildedrose

import com.gildedrose.item.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class GildedRoseTest {

    @Test
    fun `Sulfuras should not be updated`() {
        // Arrange
        val sellIn = 0
        val quality = Sulfuras.quality
        val items = arrayOf(Item("Sulfuras, Hand of Ragnaros", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality)
        assertThat(item.sellIn).isEqualTo(sellIn)
    }

    @Test
    fun `Aged Brie should increase quality by 2 if sellIn is less than 0`() {
        // Arrange
        val sellIn = AgedBrie.DAY_FROM_INCREASE_QUALITY_BY_TWO
        val quality = 20
        val items = arrayOf(Item("Aged Brie", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality + 2)
        assertThat(item.sellIn).isLessThan(sellIn)
    }

    @Test
    fun `Backstage should increase quality by 2 if sellIn is less than or equal to 10`() {
        // Arrange
        val sellIn = Backstage.DAYS_LEFT_TO_INCREASE_QUALITY_BY_TWO
        val quality = 20
        val items = arrayOf(Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality + 2)
        assertThat(item.sellIn).isLessThanOrEqualTo(sellIn)
    }

    @Test
    fun `Backstage should increase quality by 3 if sellIn is less than or equal to 5`() {
        // Arrange
        val sellIn = Backstage.DAYS_LEFT_TO_INCREASE_QUALITY_BY_THREE
        val quality = 20
        val items = arrayOf(Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality + 3)
        assertThat(item.sellIn).isLessThanOrEqualTo(sellIn)
    }

    @Test
    fun `Backstage should have quality 0 if sellIn is less than 0`() {
        // Arrange
        val sellIn = Irregular.SELL_EXPIRATION
        val quality = 20
        val items = arrayOf(Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(Backstage.QUALITY_AFTER_EXPIRATION)
        assertThat(item.sellIn).isLessThan(sellIn)
    }

    @Test
    fun `Conjured should decrease quality twice as fast as regular item`() {
        // Arrange
        val sellIn = Regular.SELL_EXPIRATION
        val quality = 20
        val items = arrayOf(
            Item("Elixir of the Mongoose", sellIn, quality),
            Item("Conjured Mana Cake", sellIn, quality)
        )
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val regularItem = items[0]
        val conjuredItem = items[1]
        assertThat(conjuredItem.quality).isEqualTo(regularItem.quality - 2)
    }

    @Test
    fun `Item should decrease quality`() {
        // Arrange
        val sellIn = 10
        val quality = 50
        val items = arrayOf(Item("Elixir of the Mongoose", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality - 1)
    }

    @Test
    fun `Item should decrease sellIn`() {
        // Arrange
        val sellIn = 10
        val quality = 50
        val items = arrayOf(Item("Elixir of the Mongoose", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.sellIn).isEqualTo(sellIn - 1)
    }

    @Test
    fun `Item should decrease quality by 2 if sellIn is less than 0`() {
        // Arrange
        val sellIn = Regular.SELL_EXPIRATION
        val quality = 50
        val items = arrayOf(Item("Elixir of the Mongoose", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality - 2)
        assertThat(item.sellIn).isLessThan(sellIn)
    }

    @Test
    fun `Item should not decrease quality less than 0`() {
        // Arrange
        val sellIn = 10
        val quality = Regular.QUALITY_MIN
        val items = arrayOf(Item("Elixir of the Mongoose", sellIn, quality))
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        val item = items[0]
        assertThat(item.quality).isEqualTo(quality)
    }

    @Test
    fun `Item should not increase quality greater than 50`() {
        // Arrange
        val sellIn = 10
        val quality = Irregular.QUALITY_MAX
        val items = arrayOf(
            Item("Aged Brie", sellIn, quality),
            Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality),
        )
        val app = GildedRose(items)

        // Act
        app.updateQuality()

        // Assert
        for (item in items) {
            assertThat(item.quality).isEqualTo(quality)
        }
    }
}
